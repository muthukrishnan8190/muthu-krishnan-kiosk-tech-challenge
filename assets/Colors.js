'use strict';

const colors = {
  white: '#FFFFFF',
  black: '#000000',
  lightGrey: '#e8e8ed',
  grey: '#808080',
  green: '#55d209',
  lightGreen: '#CCF2B5',
  darkGreen: '#4ac000',
  red: '#993333',
  transparent: 'transparent',
  trasparentOverlay: 'rgba(0, 0, 0, 0.5)',
  grayBackground:'#f1f0f2',
  darkViolet:'#2B1F52',
  violet:'#3f2a73'
};

export default colors;
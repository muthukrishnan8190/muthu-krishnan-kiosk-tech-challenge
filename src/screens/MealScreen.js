import React from 'react'
import { SafeAreaView } from 'react-native'
import customData from '../../server/data/menu.json'
import Header from '../components/Header'
//const customData = require('../../server/data/menu.json')


export default class MealScreen extends React.Component {

    state = {
        menu: customData.data.MenuGroups
    }
    constructor() {
        super()
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Header menuList = {this.state.menu}/>
            </SafeAreaView>
        )
    }
}

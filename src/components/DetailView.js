import React from 'react'
import { View, Text, StyleSheet,Image, FlatList } from 'react-native'
import Colors from '../../assets/Colors'


export default class DetailView extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        console.log('this.props.listItems',this.props.listItems)
        return (
            <View style={styles.contentContainer}>
                <Text style={{ fontSize: 40 }}>{this.props.title}</Text>
                <FlatList
                    data={this.props.listItems}
                    renderItem={({ item }) => (
                        <View style={styles.listView}>
                            <View style={styles.contentView}>
                            <Text style= {styles.titleText}>{item.name}</Text>
                                <Text>{`$ ${item.price}`}</Text>
                            </View>
                            <View style={styles.imageContainer}>
                        <Image style={styles.image} source={{ uri: `https://via.placeholder.com/300.png?text=${this.props.title}`}} />
                        </View>
                        </View>
                    )}
                    //Setting the number of column
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}

                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        padding: 15,
        width: '70%'
    },
    listView: {
        width: '48%',
        flexDirection: 'row',
        backgroundColor: Colors.white,
        height: 150,
        margin: 10,
        borderRadius: 15,
        overflow:'hidden',

    },
    contentView:{
        width: '70%',
        padding:20,
        justifyContent:'space-between'
    },
    imageContainer: {
        width:'30%',
        borderTopRightRadius:15,
        borderBottomRightRadius:15
    },
    image:{
        width:'100%',
        height:'100%',
    },
    titleText: {
    marginLeft:10,
    fontSize:20,
    color:Colors.black
    },


})
import React from 'react'
import { View, Text, StyleSheet, ScrollView, Dimensions, TouchableOpacity } from 'react-native'
import DetailView from '../components/DetailView'
import Colors from '../../assets/Colors'

export default class TabList extends React.Component {

    constructor() {
        super()
        this.state = {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            selectedIndex: 0,
        };
        Dimensions.addEventListener("change", (e) => {
            this.setState(e.window);
        })
    }
    constructSubCategoryButtons = () => {
        let buttonComponents = this.props.content.map((item, index) =>

            <TouchableOpacity style={styles.touchableView} onPress={() => this.buttonClickHandler(index)}>
                <Text style={{ color: 'white', fontSize: 20 }}>{item.name}</Text>
                <View style={{ ...styles.buttonView, ...(this.state.selectedIndex === index && styles.selectedButtonView)}}></View>
                
            </TouchableOpacity>)

        return buttonComponents

    }

    buttonClickHandler = (index) => {
        this.setState({
            selectedIndex: index,
        })
    }

    render() {
        return (
            <View style={{ flex: 1 ,backgroundColor:Colors.grayBackground}}>
                <View style={[{ width: this.state.width, height: 70}, styles.buttonContainer]}>
                    <ScrollView style={{flex:1,}}horizontal={true}>
                    {this.constructSubCategoryButtons()}
                    </ScrollView>
                </View>

                <View style={{ flex: 1,alignContent:'space-around'}}>
                    <DetailView listItems={this.props.content[this.state.selectedIndex].items} title={this.props.content[this.state.selectedIndex].name}/>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    buttonContainer: {
        backgroundColor: Colors.darkViolet,
        flexDirection: 'row'
    },
    touchableView: {
        marginHorizontal: 20,
        alignContent: "center",
        justifyContent: 'center',
        alignSelf: 'flex-end'
    },
    buttonView: {
         height: 6,
          backgroundColor: Colors.transparent,
           marginTop: 20 

    },selectedButtonView: {
        backgroundColor: 'white',
    }
})
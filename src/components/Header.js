import React from 'react'
import {View, Text,StyleSheet,Dimensions,TouchableOpacity} from 'react-native'
import TabList from './TabList'
import Colors from '../../assets/Colors'


export default class Header extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            list:this.props.menuList,
            items : this.props.menuList[0],
            selectedIndex: 0
        };
        Dimensions.addEventListener("change", (e) => {
            this.setState(e.window);
        })
    }
    constructTabButtons = () => {
        let buttonComponents = this.props.menuList.map((item, index) => 

                    <TouchableOpacity style={styles.touchableView} onPress = {()=>this.buttonClickHandler(index)}>
                    <Text style={{ ...styles.headerText, ...(this.state.selectedIndex === index && styles.selectedHewaderText)}}>{item.name}</Text>
                    </TouchableOpacity>)

        return buttonComponents    

    }

    buttonClickHandler = (index) => {

        this.setState({
            selectedIndex: index,
           items:this.state.list[index]
        })
       }

    render() {
        return(
            <View style= {{flex:1}}> 
            <View style={[{width: this.state.width,height:70},styles.Container]}>
            {this.constructTabButtons()}
             </View>
             <TabList content={this.state.items.categories}/>
             </View>
        )
    }
}

const screenWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
    Container: {
        backgroundColor:Colors.violet,
        flexDirection:'row',
        justifyContent:'flex-start'
    },
    touchableView:{
        marginHorizontal:20,
        alignContent:"center",
        justifyContent:'center',
    },
    headerText:{
        color: Colors.white,
        fontSize:20,
        textTransform:'uppercase',
        fontWeight:'normal'
    },
    selectedHewaderText: {
       fontWeight:'bold',
       fontSize:22 
    }
})
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import MealScreen from './src/screens/MealScreen'

class App extends React.Component {
render(){
  return(
    <MealScreen />
  )
}
}
export default App